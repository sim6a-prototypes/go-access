package main

import (
	"go-access/internal/app/access/infrastructure/startup"
)

func main() {
	startup.Do()
}
