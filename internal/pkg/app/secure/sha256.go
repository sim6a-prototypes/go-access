package secure

import (
	"crypto/sha256"
)

func Sha256(key string) []byte {
	hash := sha256.Sum256([]byte(key))
	return hash[:]
}
