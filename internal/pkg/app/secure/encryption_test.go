package secure_test

import (
	"go-access/internal/pkg/app/secure"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncryptAndDecrypt(t *testing.T) {
	var tests = []struct {
		name     string
		data     string
		password string
	}{
		{
			name:     "json data",
			data:     `{"guid":"016cf4eb-3aeb-0684-6f17-9fe1ee876865"}`,
			password: "p@sswor!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ciphertext, errEncryption := secure.Encrypt(tt.data, tt.password)
			assert.NoError(t, errEncryption)
			plainText, errDecryption := secure.Decrypt(ciphertext, tt.password)
			assert.NoError(t, errDecryption)
			assert.Equal(t, tt.data, plainText)
		})
	}
}
