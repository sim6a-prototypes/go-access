package secure

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"

	"emperror.dev/errors"
)

func Encrypt(data string, passphrase string) (Hex, error) {
	hash := Sha256(passphrase)
	block, _ := aes.NewCipher([]byte(hash))
	gcm, errGCM := cipher.NewGCM(block)
	if errGCM != nil {
		return NewEmptyHex(), errGCM
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, errReading := io.ReadFull(rand.Reader, nonce); errReading != nil {
		return NewEmptyHex(), errReading
	}
	ciphertext := gcm.Seal(nonce, nonce, []byte(data), nil)
	return ciphertext, nil
}

func Decrypt(data Hex, passphrase string) (string, error) {
	key := Sha256(passphrase)
	block, errCipher := aes.NewCipher([]byte(key))
	if errCipher != nil {
		return "", errCipher
	}
	gcm, errGCM := cipher.NewGCM(block)
	if errGCM != nil {
		return "", errGCM
	}
	nonceSize := gcm.NonceSize()
	if len(data) <= nonceSize {
		return "", errors.NewWithDetails("encrypted data length is too small",
			"encrypted_data", data.String,
		)
	}
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plainText, errOpening := gcm.Open(nil, nonce, ciphertext, nil)
	if errOpening != nil {
		return "", errOpening
	}
	return string(plainText), nil
}
