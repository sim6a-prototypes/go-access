package secure

import (
	"encoding/hex"

	"emperror.dev/errors"
)

type Hex []byte

func NewEmptyHex() Hex {
	return []byte{}
}

func NewHex(s string) (Hex, error) {
	h, err := hex.DecodeString(s)
	if err != nil {
		return NewEmptyHex(), errors.WrapWithDetails(err, "cannot decode hex string",
			"string", s,
		)
	}
	return h, nil
}

func (h Hex) String() string {
	return hex.EncodeToString(h)
}
