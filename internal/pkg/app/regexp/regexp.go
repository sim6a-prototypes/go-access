package regexp

import "regexp"

var (
	/*
		Author: Eric Lebetsamer
		Original: http://regexlib.com/REDetails.aspx?regexp_id=35
	*/
	Email = regexp.MustCompile(`^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$`)

	/*
		Validates a simple ip v4 address.
		Including 0.0.0.0 or 255.255.255.255. Leading 0 is and numbers above 255 are forbitten.
		Author: Daniel Adam
		Original: http://regexlib.com/REDetails.aspx?regexp_id=1139
	*/
	IPv4 = regexp.MustCompile(`^((\d|[1-9]\d|2[0-4]\d|25[0-5]|1\d\d)(?:\.(\d|[1-9]\d|2[0-4]\d|25[0-5]|1\d\d)){3})$`)

	/*
		Author: Jeff Howden
		Original: http://regexlib.com/REDetails.aspx?regexp_id=275
	*/
	Domain = regexp.MustCompile(`^([a-z0-9]+([\-a-z0-9]*[a-z0-9]+)?\.){0,}([a-z0-9]+([\-a-z0-9]*[a-z0-9]+)?){1,63}(\.[a-z0-9]{2,7})+$`)

	/*
		Match all alphanumeric character and predefined wild characters.
		Password must consists of at least 8 characters and not more than 15 characters.
		Author: Lawson Law
		Original: http://regexlib.com/REDetails.aspx?regexp_id=213
	*/
	Password = regexp.MustCompile(`^([a-zA-Z0-9@*#]{8,15})$`)

	/*
		This Pattern is used to validate Login Name with . and _ seprators only.
		Author: Amol Pande
		Original: http://regexlib.com/REDetails.aspx?regexp_id=1490
	*/
	Login = regexp.MustCompile(`^([a-zA-Z](?:(?:(?:\w[\.\_]?)*)\w)+)([a-zA-Z0-9])$`)
)
