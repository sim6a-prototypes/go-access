package ozzovalidation

import (
	"regexp"
	"strings"

	"emperror.dev/errors"
	validation "github.com/go-ozzo/ozzo-validation/v4"
)

func OzzoErrorsToEmperror(err error) error {
	errs, _ := err.(validation.Errors)
	errorList := make([]error, 0, len(errs))
	for _, e := range errs {
		message, details := parseInlineDetails(e.Error())
		errorList = append(errorList,
			errors.NewWithDetails(message, details...))
	}
	return errors.Combine(errorList...)
}

func parseInlineDetails(fullMessage string) (string, []interface{}) {
	delimeter := "+"
	parts := strings.Split(fullMessage, delimeter)
	message := formatOzzoMessage(parts[0])
	detailsInParts := parts[1:]
	details := make([]interface{}, 0, len(detailsInParts))
	for _, part := range detailsInParts {
		details = append(details, part)
	}
	return message, details
}

var (
	eachPrefix = regexp.MustCompile(`\d*:\s`)
)

func formatOzzoMessage(message string) string {
	return eachPrefix.ReplaceAllString(message, "")
}
