package ozzovalidation_test

import (
	"testing"

	ozzo "go-access/internal/pkg/third_party/ozzovalidation"

	"emperror.dev/errors"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/stretchr/testify/assert"
)

func TestOzzoErrorsToEmperror(t *testing.T) {
	delimeter := "+"

	ozzoErrs := validation.Errors{
		"1": errors.New("message-a" + delimeter + "detail-a1" + delimeter + "detail-a2"),
		"2": errors.New("0: message-b" + delimeter + "detail-b1" + delimeter + "detail-b2"),
	}

	expectedErrors := errors.GetErrors(errors.Combine(
		errors.NewWithDetails("message-a", "detail-a1", "detail-a2"),
		errors.NewWithDetails("message-b", "detail-b1", "detail-b2"),
	))
	actualErrors := errors.GetErrors(ozzo.OzzoErrorsToEmperror(ozzoErrs))

	for i := 0; i < len(actualErrors); i++ {
		actualError := actualErrors[i]
		expectedError := expectedErrors[i]

		assert.Equal(t, expectedError.Error(), actualError.Error())

		actualDetails := errors.GetDetails(actualError)
		expectedDetails := errors.GetDetails(expectedError)
		for j := 0; j < len(actualDetails); j++ {
			actualDetail := actualDetails[j]
			expectedDetail := expectedDetails[j]

			assert.Equal(t, expectedDetail.(string), actualDetail.(string))
		}
	}
}
