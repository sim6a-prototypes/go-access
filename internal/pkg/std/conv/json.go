package conv

import (
	"bytes"
	"encoding/json"
)

func CompactJSON(jsonBytes []byte) (string, error) {
	buffer := bytes.Buffer{}
	if err := json.Compact(&buffer, jsonBytes); err != nil {
		return "", err
	}
	return buffer.String(), nil
}
