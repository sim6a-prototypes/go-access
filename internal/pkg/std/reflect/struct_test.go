package reflect_test

import (
	"go-access/internal/pkg/std/reflect"
	"testing"
)

func TestExtractTagValueFromStruct(t *testing.T) {
	type user struct {
		Name string `json:"name"`
	}

	u := user{}

	expectedTagValue := "name"
	actualTagValue := reflect.ExtractTagValueFromStruct("json", &u, &u.Name)

	if actualTagValue != expectedTagValue {
		t.Errorf("TestExtractTagValueFromStruct() = %v, want %v",
			actualTagValue, expectedTagValue)
	}
}
