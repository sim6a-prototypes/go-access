package reflect

import (
	"go-access/third_party/qiangxue"
	"reflect"
	"strings"
)

func ExtractTagValue(tagName string, field reflect.StructField) string {
	name := strings.SplitN(field.Tag.Get(tagName), ",", 2)[0]
	if name == "-" {
		return ""
	}
	return name
}

func ExtractTagValueFromStruct(tagName string, structPtr, fieldPtr interface{}) string {
	structValue := reflect.ValueOf(structPtr).Elem()
	fieldValue := reflect.ValueOf(fieldPtr)
	field := qiangxue.FindStructField(structValue, fieldValue)
	return ExtractTagValue(tagName, *field)
}
