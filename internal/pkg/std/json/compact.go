package json

import (
	"bufio"
	"strings"
)

func CompactString(j string) string {
	lines, _ := stringToLines(j)
	builder := strings.Builder{}
	for _, line := range lines {
		builder.WriteString(strings.TrimSpace(line))
	}
	return builder.String()
}

func stringToLines(s string) (lines []string, err error) {
	scanner := bufio.NewScanner(strings.NewReader(s))
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	err = scanner.Err()
	return
}
