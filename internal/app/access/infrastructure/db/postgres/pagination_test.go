package postgres_test

import (
	"go-access/internal/app/access/infrastructure/db/postgres"
	"testing"
)

func TestComputeLimitAndOffset(t *testing.T) {
	type args struct {
		pageNumber   int
		itemsPerPage int
	}
	tests := []struct {
		name       string
		args       args
		wantLimit  int
		wantOffset int
	}{
		{
			name: "the first page, page isn't specified", args: args{
				pageNumber:   0,
				itemsPerPage: 5,
			},
			wantLimit:  5,
			wantOffset: 0,
		},
		{
			name: "the first page", args: args{
				pageNumber:   1,
				itemsPerPage: 5,
			},
			wantLimit:  5,
			wantOffset: 0,
		},
		{
			name: "the second page", args: args{
				pageNumber:   2,
				itemsPerPage: 5,
			},
			wantLimit:  5,
			wantOffset: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			limit, offset := postgres.ComputeLimitAndOffset(tt.args.pageNumber, tt.args.itemsPerPage)
			if limit != tt.wantLimit {
				t.Errorf("ComputeLimitAndOffset() limit = %v, want %v", limit, tt.wantLimit)
			}
			if offset != tt.wantOffset {
				t.Errorf("ComputeLimitAndOffset() offset = %v, want %v", offset, tt.wantOffset)
			}
		})
	}
}
