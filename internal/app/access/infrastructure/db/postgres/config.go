package postgres

import "fmt"

type Config struct {
	User     string
	Password string
	DBName   string
}

func (c *Config) ConnectionString() string {
	return fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=disable",
		"postgres", 5432, c.User, c.DBName, c.Password)
}
