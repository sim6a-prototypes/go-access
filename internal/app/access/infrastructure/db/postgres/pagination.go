package postgres

func ComputeLimitAndOffset(pageNumber, itemsPerPage int) (int, int) {
	if pageNumber == 0 {
		return itemsPerPage, 0
	}
	return itemsPerPage, (pageNumber - 1) * itemsPerPage
}
