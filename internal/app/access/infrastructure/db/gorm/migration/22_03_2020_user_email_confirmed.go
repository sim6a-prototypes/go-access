package migration

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func userEmailConfirmed() *gormigrate.Migration {
	return &gormigrate.Migration{
		ID: "22-03-2020 add 'email_confirmed' field to users table",
		Migrate: func(tx *gorm.DB) error {
			type User struct {
				EmailConfirmed bool
			}
			return tx.AutoMigrate(&User{}).Error
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.DropColumn("email_confirmed").Error
		},
	}
}
