package migration

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func userPasswordHash() *gormigrate.Migration {
	// see https://sniptools.com/databases/resize-a-column-in-a-postgresql-table-without-changing-data/
	// to more details
	return &gormigrate.Migration{
		ID: "12-03-2020 increase user password field size",
		Migrate: func(tx *gorm.DB) error {
			return tx.Exec(`UPDATE pg_attribute SET atttypmod = 64+4
				WHERE attrelid = 'users'::regclass
				AND attname = 'password';
			`).Error
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Exec(`UPDATE pg_attribute SET atttypmod = 50+4
				WHERE attrelid = 'users'::regclass
				AND attname = 'password';
			`).Error
		},
	}
}
