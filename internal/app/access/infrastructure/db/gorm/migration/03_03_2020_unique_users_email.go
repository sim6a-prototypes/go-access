package migration

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func uniqueUserEmail() *gormigrate.Migration {
	return &gormigrate.Migration{
		ID: "03-03-2020 unique users email",
		Migrate: func(tx *gorm.DB) error {
			return tx.Table("users").AddUniqueIndex("idx_users_email", "email").Error
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Table("users").RemoveIndex("idx_users_email").Error
		},
	}
}
