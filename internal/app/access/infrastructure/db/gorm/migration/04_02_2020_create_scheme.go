package migration

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func createScheme() *gormigrate.Migration {
	return &gormigrate.Migration{
		ID: "04-02-2020 create scheme",
		Migrate: func(tx *gorm.DB) error {

			type User struct {
				gorm.Model

				Login    string `gorm:"type:varchar(50)"`
				Password string `gorm:"type:varchar(50)"`
				Email    string `gorm:"type:varchar(100)"`
			}

			return tx.AutoMigrate(&User{}).Error
		},
	}
}
