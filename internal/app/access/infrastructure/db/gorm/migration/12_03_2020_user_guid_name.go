package migration

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func userGuidName() *gormigrate.Migration {
	return &gormigrate.Migration{
		ID: "12-03-2020 user guid, first and last name",
		Migrate: func(tx *gorm.DB) error {
			type User struct {
				GUID      string `gorm:"type:varchar(36)"`
				FirstName string `gorm:"type:varchar(50)"`
				LastName  string `gorm:"type:varchar(50)"`
			}
			return tx.Table("users").
				DropColumn("login").
				AutoMigrate(&User{}).
				Error
		},
		Rollback: func(tx *gorm.DB) error {
			type User struct {
				Login string `gorm:"type:varchar(50)"`
			}

			return tx.Table("users").
				DropColumn("guid").
				DropColumn("first_name").
				DropColumn("last_name").
				AutoMigrate(&User{}).
				Error
		},
	}
}
