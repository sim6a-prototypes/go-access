package migration

import (
	"gopkg.in/gormigrate.v1"
)

func GetAll() []*gormigrate.Migration {
	return []*gormigrate.Migration{
		createScheme(),
		uniqueUserEmail(),
		userGuidName(),
		userPasswordHash(),
		userEmailConfirmed(),
	}
}
