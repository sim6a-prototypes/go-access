package user

import (
	"go-access/internal/app/access/app/log"
	"go-access/internal/app/access/domain/model/pagination"
	"go-access/internal/app/access/domain/model/user"
	"go-access/internal/app/access/infrastructure/db/postgres"

	"emperror.dev/errors"

	"github.com/jinzhu/gorm"
)

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		db: db,
	}
}

type Repository struct {
	db *gorm.DB
}

func (r *Repository) FindByEmailAndPassword(email, password string) (*user.User, error) {
	u := &user.User{}
	err := r.db.Where(&user.User{Email: email, Password: password}).First(u).Error
	if err != nil {
		// TODO: move 'record not found' checking on more higher layer
		if err == gorm.ErrRecordNotFound {
			return nil, user.ErrUnknownUser
		}
		return nil, errors.WithDetails(err, log.Database, log.Postgres)
	}
	return u, nil
}

func (r *Repository) Add(u *user.User) error {
	if err := r.db.Create(u).Error; err != nil {
		return errors.WithDetails(err, log.Database, log.Postgres)
	}
	return nil
}

func (r *Repository) GetPage(page pagination.Page) (user.Collection, error) {
	var users user.Collection
	limit, offset := postgres.ComputeLimitAndOffset(page.Number, page.Size)
	err := r.db.
		Limit(limit).
		Offset(offset).
		Find(&users).
		Error
	if err != nil {
		return nil, errors.WithDetails(err, log.Database, log.Postgres)
	}
	return users, nil
}

func (r *Repository) Count() (int, error) {
	var total int
	if err := r.db.Model(&user.User{}).Count(&total).Error; err != nil {
		return 0, errors.WithDetails(err, log.Database, log.Postgres)
	}
	return total, nil
}

func (r *Repository) FindByEmail(email string) (user.Collection, error) {
	var users user.Collection
	err := r.db.Where(&user.User{Email: email}).
		Find(&users).
		Error
	if err != nil {
		return nil, errors.WithDetails(err, log.Database, log.Postgres)
	}
	return users, nil
}

func (r *Repository) FindByGUID(userGUID string) (*user.User, error) {
	u := &user.User{}
	err := r.db.Where(&user.User{GUID: userGUID}).First(u).Error
	if err != nil {
		return nil, errors.WithDetails(err, log.Database, log.Postgres)
	}
	return u, nil
}

func (r *Repository) Update(u *user.User) error {
	return r.db.Save(u).Error
}
