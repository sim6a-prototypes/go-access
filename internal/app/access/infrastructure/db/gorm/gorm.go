package gorm

import (
	"go-access/internal/app/access/app/log"
	"go-access/internal/app/access/infrastructure/db/gorm/migration"
	"go-access/internal/app/access/infrastructure/db/postgres"
	"time"

	"emperror.dev/errors"
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"

	// postgres driver
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func ConnectToPostgres(cfg postgres.Config, logger log.Logger) (*gorm.DB, func(), error) {
	var db *gorm.DB
	var err error
	const connectionError = "connection error"
	for try := 1; try <= 3; try++ {
		logger.Info("trying to connect ...",
			log.Database, log.Postgres,
		)
		if db, err = gorm.Open("postgres", cfg.ConnectionString()); err != nil {
			logger.Warn(errors.WrapWithDetails(err, connectionError,
				log.Database, log.Postgres))
			time.Sleep(time.Duration(try) * time.Second)
		} else {
			break
		}
	}
	if err != nil {
		return nil, nil, errors.NewWithDetails(connectionError,
			log.Database, log.Postgres,
		)
	}
	close := func() {
		if err := db.Close(); err != nil {
			logger.Warn(errors.WrapWithDetails(err, "an error occurred during connection closing",
				log.Database, log.Postgres,
			))
		}
	}
	return db, close, nil
}

type Config struct {
	EnableLogging bool
}

func Configure(db *gorm.DB, cfg Config) {
	db.LogMode(cfg.EnableLogging)
}

func ApplyMigrations(db *gorm.DB) error {
	migrator := gormigrate.New(db, gormigrate.DefaultOptions, migration.GetAll())
	if err := migrator.Migrate(); err != nil {
		return errors.WrapWithDetails(err, "migration error",
			log.Database, log.Postgres,
		)
	}
	return nil
}
