package logrus

import (
	"go-access/internal/app/access/app/log"
	"go-access/internal/app/access/domain/model/event"
	"go-access/internal/app/access/domain/model/user"
)

func (l *Logger) Handle(evnt event.Event) {
	if handle, ok := l.eventHandlers[evnt.Type()]; ok {
		handle(evnt)
	}
}

func (l *Logger) initEventHandlers() {
	l.initUserEventHandlers()
}

func (l *Logger) initUserEventHandlers() {
	/* user registered */
	l.eventHandlers[event.UserRegistered] = func(evnt event.Event) {
		concreteEvent := evnt.(*user.Registered)
		l.Info("a new user registered",
			"guid", concreteEvent.UserGUID,
			log.EventType, concreteEvent.Type().String(),
			log.RequestTraceID, concreteEvent.RequestTraceID,
		)
	}

	/* user authenticated */
	l.eventHandlers[event.UserAuthenticated] = func(evnt event.Event) {
		concreteEvent := evnt.(*user.Authenticated)
		l.Info("a user authenticated",
			"guid", concreteEvent.UserGUID,
			log.EventType, concreteEvent.Type().String(),
			log.RequestTraceID, concreteEvent.RequestTraceID,
		)
	}

	/* user list retrieved */
	l.eventHandlers[event.UserListRetrieved] = func(evnt event.Event) {
		concreteEvent := evnt.(*user.ListRetrieved)
		l.Info("user list retrieved",
			"recipientGuid", concreteEvent.RecipientGUID,
			log.EventType, concreteEvent.Type().String(),
			log.RequestTraceID, concreteEvent.RequestTraceID,
		)
	}

	/* user activated */
	l.eventHandlers[event.UserActivated] = func(evnt event.Event) {
		concreteEvent := evnt.(*user.Activated)
		l.Info("user activated",
			"guid", concreteEvent.UserGUID,
			log.EventType, concreteEvent.Type().String(),
			log.RequestTraceID, concreteEvent.RequestTraceID,
		)
	}
}
