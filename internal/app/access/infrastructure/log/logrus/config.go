package logrus

import (
	"os"

	"github.com/sirupsen/logrus"
)

type Config struct {
	Formatter string
}

func (c *Config) CreateFormatter() logrus.Formatter {
	if c.Formatter == "json" {
		return &logrus.JSONFormatter{}
	}
	return &logrus.TextFormatter{}
}

func Configure(logger *Logger, cfg Config) {
	logger.core.SetOutput(os.Stdout)
	logger.core.SetFormatter(cfg.CreateFormatter())
}
