package logrus

import (
	"go-access/internal/app/access/domain/model/event"

	"emperror.dev/errors"
	"emperror.dev/errors/utils/keyval"
	"github.com/sirupsen/logrus"
)

type Logger struct {
	core          *logrus.Logger
	eventHandlers map[event.Type]func(event.Event)
}

func New() *Logger {
	l := &Logger{
		core:          logrus.New(),
		eventHandlers: make(map[event.Type]func(event.Event)),
	}
	l.initEventHandlers()
	return l
}

type logFunc func(args ...interface{})

func getErrorFunc(logger logrus.FieldLogger) logFunc {
	return logger.Error
}

func getWarnFunc(logger logrus.FieldLogger) logFunc {
	return logger.Warn
}

func (l *Logger) logEmperror(getLogFunc func(logrus.FieldLogger) logFunc, err error) {
	logErr := func(err error) {
		if details := errors.GetDetails(err); len(details) > 0 {
			getLogFunc(l.core.WithFields(keyval.ToMap(details)))(err.Error())
		} else {
			getLogFunc(l.core)(err.Error())
		}
	}

	type errorCollection interface {
		Errors() []error
	}

	if errs, ok := err.(errorCollection); ok {
		for _, e := range errs.Errors() {
			logErr(e)
		}
	} else {
		logErr(err)
	}
}

/* Information */

func (l *Logger) Info(msg string, details ...interface{}) {
	l.core.WithFields(keyval.ToMap(details)).Info(msg)
}

/* Warning */

func (l *Logger) Warn(err error) {
	l.logEmperror(getWarnFunc, err)
}

/* Error */

func (l *Logger) Error(err error) {
	l.logEmperror(getErrorFunc, err)
}
