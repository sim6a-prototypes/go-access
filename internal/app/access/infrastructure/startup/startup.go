package startup

import (
	"go-access/internal/app/access/app/log"
	notifier "go-access/internal/app/access/app/notifier/email"
	appUser "go-access/internal/app/access/app/service/user"
	"go-access/internal/app/access/domain/model/event"
	"go-access/internal/app/access/domain/model/user"
	"go-access/internal/app/access/infrastructure/configurator"
	"go-access/internal/app/access/infrastructure/db/gorm"
	gormUser "go-access/internal/app/access/infrastructure/db/gorm/user"
	"go-access/internal/app/access/infrastructure/email"
	"go-access/internal/app/access/infrastructure/log/logrus"
	"go-access/internal/app/access/resource"
)

func Do() {
	logger := logrus.New()
	cfg := configurator.New()
	if err := cfg.ReadConfig(); err != nil {
		logger.Error(err)
		return
	}

	logrus.Configure(logger, cfg.Logrus())
	cfg.Log(logger)

	connection, closeConnection, err := gorm.ConnectToPostgres(cfg.PostgresConnection(), logger)
	if err != nil {
		logger.Error(err)
		return
	}
	defer closeConnection()
	gorm.Configure(connection, cfg.Gorm())
	logger.Info("connection established",
		log.Database, log.Postgres,
	)

	if err := gorm.ApplyMigrations(connection); err != nil {
		logger.Error(err)
		return
	}
	logger.Info("migrations applied",
		log.Database, log.Postgres,
	)

	emailSender := email.NewSender(cfg.Email())
	emailNotifier := notifier.NewNotifier(cfg.EmailNotifier(), emailSender, logger)
	compositeObserver := event.NewCompositeObserver()
	compositeObserver.AddObserver(emailNotifier)
	compositeObserver.AddObserver(logger)
	userRepository := gormUser.NewRepository(connection)
	userService := user.NewService(userRepository, compositeObserver, cfg.EncryptionKey())
	appUserService := appUser.NewService(userService, emailNotifier, cfg.ServiceAddress())

	restAPI := resource.NewRestAPI(cfg.Server())
	resource.ConfigureRestAPI(restAPI, cfg.Server(), logger)
	resource.InjectServices(restAPI, appUserService)
	resource.RunRestAPI(restAPI, cfg.Server(), logger)
}
