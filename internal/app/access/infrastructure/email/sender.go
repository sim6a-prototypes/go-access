package email

import (
	"go-access/internal/app/access/app/email"
	"go-access/internal/pkg/std/conv"
	"net"
	"net/smtp"
	"time"

	"emperror.dev/errors"
)

type Config struct {
	Address    string
	Password   string
	Host       string
	Port       int
	TimeoutSec int
}

func NewSender(cfg Config) *Sender {
	return &Sender{
		cfg: cfg,
	}
}

type Sender struct {
	cfg Config
}

// TODO: add headers https://github.com/tangingw/go_smtp
func (s *Sender) SendByTemplate(tpl email.Template, subject string, receivers []string) error {
	auth := smtp.PlainAuth("", s.cfg.Address, s.cfg.Password, s.cfg.Host)
	request := Request{
		Auth:    auth,
		From:    s.cfg.Address,
		To:      receivers,
		Address: net.JoinHostPort(s.cfg.Host, conv.IntToStr(s.cfg.Port)),
		Subject: subject,
		Timeout: time.Duration(s.cfg.TimeoutSec) * time.Second,
	}
	if err := request.ParseTemplate(tpl.FileName, tpl.Data); err != nil {
		return errors.WrapWithDetails(err, "email template parsing error",
			"template_name", tpl.FileName,
		)
	}
	if err := request.SendEmail(); err != nil {
		return errors.Wrap(err, "email sending error")
	}
	return nil
}
