package email

import (
	"bytes"
	"net/smtp"
	"text/template"
	"time"

	"emperror.dev/errors"
)

type Request struct {
	Timeout time.Duration
	Auth    smtp.Auth
	From    string
	To      []string
	Address string
	Subject string
	Body    string
}

func (r *Request) ParseTemplate(templateFileName string, data interface{}) error {
	tpl, err := template.ParseFiles(templateFileName)
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	if err := tpl.Execute(buf, data); err != nil {
		return err
	}
	r.Body = buf.String()
	return nil
}

func (r *Request) SendEmail() error {
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "Subject: " + r.Subject + "\n"
	message := []byte(subject + mime + "\n" + r.Body)

	f := func(errChan chan error) {
		errChan <- smtp.SendMail(r.Address, r.Auth, r.From, r.To, message)
	}
	return r.sendEmailWithTimeout(r.Timeout, f)
}

func (r *Request) sendEmailWithTimeout(timeout time.Duration, f func(chan error)) error {
	c := make(chan error, 1)

	go f(c)

	select {
	case err := <-c:
		return err
	case <-time.After(timeout):
		return errors.NewWithDetails("timeout exceeded", "timeout", timeout)
	}
}
