package configurator

import (
	"fmt"
	"go-access/internal/app/access/app/log"
	pkgReflect "go-access/internal/pkg/std/reflect"
	ozzo "go-access/internal/pkg/third_party/ozzovalidation"
	"reflect"

	"emperror.dev/errors"
	"github.com/go-ozzo/ozzo-validation/is"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/kelseyhightower/envconfig"
)

func New() *Configurator {
	return &Configurator{
		cfg: &Config{},
	}
}

type Configurator struct {
	cfg *Config
}

func (c *Configurator) ReadConfig() error {
	if err := envconfig.Process("ACCESS", c.cfg); err != nil {
		return errors.Wrap(err, log.Configuration+": invalid scheme")
	}

	if err := c.validate(); err != nil {
		return err
	}

	return nil
}

const envconfigTag = "envconfig"

func (c *Configurator) Log(logger log.Logger) {
	structValue := reflect.ValueOf(c.cfg).Elem()
	for i := 0; i < structValue.NumField(); i++ {
		field := structValue.Type().Field(i)
		value := structValue.Field(i).Interface()
		tagValue := pkgReflect.ExtractTagValue(envconfigTag, field)
		logger.Info(log.Configuration,
			tagValue, value,
		)
	}
}

func (c *Configurator) validate() error {
	err := validation.ValidateStruct(c.cfg,
		validation.Field(&c.cfg.Host,
			is.IPv4.Error(c.error("invalid app host ip",
				&c.cfg.Host, c.cfg.Host)),
		),

		validation.Field(&c.cfg.Logrus_Formatter,
			validation.In("text", "json").
				Error(c.error("invalid logrus formatter",
					&c.cfg.Logrus_Formatter, c.cfg.Logrus_Formatter)),
		),

		validation.Field(&c.cfg.Gin_Mode,
			validation.In("test", "debug", "release").
				Error(c.error("invalid gin mode",
					&c.cfg.Gin_Mode, c.cfg.Gin_Mode)),
		),

		validation.Field(&c.cfg.Email_Host,
			is.Domain.Error(c.error("invalid email host format",
				&c.cfg.Email_Host, c.cfg.Email_Host)),
			validation.Required.Error(c.error("email host is required",
				&c.cfg.Email_Host, c.cfg.Email_Host)),
		),

		validation.Field(&c.cfg.ServiceAddress,
			is.URL.Error(c.error("invalid service address format",
				&c.cfg.ServiceAddress, c.cfg.ServiceAddress)),
			validation.Required.Error(c.error("service address is required",
				&c.cfg.ServiceAddress, c.cfg.ServiceAddress)),
		),

		validation.Field(&c.cfg.EncryptionKey,
			validation.Required.Error(c.error("encryption key is required",
				&c.cfg.EncryptionKey, c.cfg.EncryptionKey)),
		),

		validation.Field(&c.cfg.Email_Port,
			validation.Required.Error(c.error("email port is required",
				&c.cfg.Email_Port, c.cfg.Email_Port)),
		),

		validation.Field(&c.cfg.Email_Password,
			validation.Required.Error(c.error("email host is required",
				&c.cfg.Email_Password, c.cfg.Email_Password)),
		),

		validation.Field(&c.cfg.Email_Address,
			is.Email.Error(c.error("invalid email address format",
				&c.cfg.Email_Address, c.cfg.Email_Address)),
			validation.Required.Error(c.error("email address is required",
				&c.cfg.Email_Address, c.cfg.Email_Address)),
		),

		validation.Field(&c.cfg.EmailNotifier_Receivers, validation.Each(
			is.Email.Error(c.error("incorrect receiver email address format",
				&c.cfg.EmailNotifier_Receivers, c.cfg.EmailNotifier_Receivers)),
		)),

		validation.Field(&c.cfg.Postgres_User,
			validation.Required.Error(c.error("postgres user name is required",
				&c.cfg.Postgres_User, c.cfg.Postgres_User)),
		),

		validation.Field(&c.cfg.Postgres_DbName,
			validation.Required.Error(c.error("postgres database name is required",
				&c.cfg.Postgres_DbName, c.cfg.Postgres_DbName)),
		),

		validation.Field(&c.cfg.Postgres_Password,
			validation.Required.Error(c.error("postgres password is required",
				&c.cfg.Postgres_Password, c.cfg.Postgres_Password)),
		),
	)
	return ozzo.OzzoErrorsToEmperror(err)
}

func (c *Configurator) error(msg string, fieldPtr, fieldValue interface{}) string {
	return fmt.Sprintf("%s+%s+%s", log.Configuration+":"+msg, c.tag(fieldPtr), fieldValue)
}

func (c *Configurator) tag(fieldPtr interface{}) string {
	return pkgReflect.ExtractTagValueFromStruct(envconfigTag, c.cfg, fieldPtr)
}
