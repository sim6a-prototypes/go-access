package configurator

import (
	emailNotifier "go-access/internal/app/access/app/notifier/email"
	"go-access/internal/app/access/infrastructure/db/gorm"
	"go-access/internal/app/access/infrastructure/db/postgres"
	"go-access/internal/app/access/infrastructure/email"
	"go-access/internal/app/access/infrastructure/log/logrus"
	"go-access/internal/app/access/resource"
	"go-access/internal/pkg/std/conv"
	"net"
)

type Config struct {
	Host string `envconfig:"HOST" default:"0.0.0.0"`
	Port int    `envconfig:"PORT" default:"8080"`

	Postgres_DbName   string `envconfig:"POSTGRES_DB"`
	Postgres_User     string `envconfig:"POSTGRES_USER"`
	Postgres_Password string `envconfig:"POSTGRES_PASSWORD"`

	Gorm_Logging bool `envconfig:"GORM_LOGGING" default:"true"`

	Logrus_Formatter string `envconfig:"LOGRUS_FORMATTER" default:"text"`

	Gin_Mode string `envconfig:"GIN_MODE" default:"debug"`

	Email_Address    string `envconfig:"EMAIL_ADDRESS"`
	Email_Password   string `envconfig:"EMAIL_PASSWORD"`
	Email_Host       string `envconfig:"EMAIL_HOST"`
	Email_Port       int    `envconfig:"EMAIL_PORT"`
	Email_TimeoutSec int    `envconfig:"EMAIL_TIMEOUT_SEC" default:"5"`

	EmailNotifier_Receivers []string `envconfig:"EMAIL_NOTIFIER_RECEIVERS"`

	ServiceAddress string `envconfig:"SERVICE_ADDRESS"`
	EncryptionKey  string `envconfig:"ENCRYPTION_KEY"`
}

func (c *Configurator) ServiceAddress() string {
	return c.cfg.ServiceAddress
}

func (c *Configurator) EncryptionKey() string {
	return c.cfg.EncryptionKey
}

func (c *Configurator) PostgresConnection() postgres.Config {
	return postgres.Config{
		DBName:   c.cfg.Postgres_DbName,
		User:     c.cfg.Postgres_User,
		Password: c.cfg.Postgres_Password,
	}
}

func (c *Configurator) Gorm() gorm.Config {
	return gorm.Config{
		EnableLogging: c.cfg.Gorm_Logging,
	}
}

func (c *Configurator) Logrus() logrus.Config {
	return logrus.Config{
		Formatter: c.cfg.Logrus_Formatter,
	}
}

func (c *Configurator) Email() email.Config {
	return email.Config{
		Address:    c.cfg.Email_Address,
		Password:   c.cfg.Email_Password,
		Host:       c.cfg.Email_Host,
		Port:       c.cfg.Email_Port,
		TimeoutSec: c.cfg.Email_TimeoutSec,
	}
}

func (c *Configurator) EmailNotifier() emailNotifier.Config {
	return emailNotifier.Config{
		Receivers: c.cfg.EmailNotifier_Receivers,
	}
}

func (c *Configurator) Server() resource.Config {
	return resource.Config{
		Address: net.JoinHostPort(c.cfg.Host, conv.IntToStr(c.cfg.Port)),
		GinMode: c.cfg.Gin_Mode,
	}
}
