package email

import (
	"go-access/internal/app/access/app/email"
	"go-access/internal/app/access/app/log"
	"go-access/internal/app/access/domain/model/event"
	"go-access/internal/app/access/domain/model/user"

	"emperror.dev/errors"
)

type Config struct {
	Receivers []string
}

func NewNotifier(cfg Config, sender email.Sender, logger log.Logger) *Notifier {
	return &Notifier{
		sender: sender,
		logger: logger,
		cfg:    cfg,
	}
}

type Notifier struct {
	sender email.Sender
	logger log.Logger
	cfg    Config
}

func (n *Notifier) Handle(evnt event.Event) {
	if len(n.cfg.Receivers) == 0 {
		return
	}
	if evnt.Type() != event.UserRegistered {
		return
	}
	concreteEvent := evnt.(*user.Registered)
	tpl := email.Template{
		FileName: "assets/html/user_registered.html",
		Data: struct {
			UserFirstName string
			UserLastName  string
		}{
			UserFirstName: concreteEvent.UserFirstName,
			UserLastName:  concreteEvent.UserLastName,
		},
	}

	go func() {
		if err := n.sender.SendByTemplate(tpl, "go-access notification", n.cfg.Receivers); err != nil {
			n.logger.Warn(errors.WrapWithDetails(err, "cannot send email notification",
				log.EventType, evnt.Type().String(),
			))
		}
	}()
}

func (n *Notifier) SendUserActivationMessage(activationLink, userEmail string) error {
	tpl := email.Template{
		FileName: "assets/html/user_activation.html",
		Data: struct {
			ActivationLink string
		}{
			ActivationLink: activationLink,
		},
	}
	if err := n.sender.SendByTemplate(tpl, "go-access user activation", []string{userEmail}); err != nil {
		return errors.Wrap(err, "cannot send email with activation link")
	}
	return nil
}
