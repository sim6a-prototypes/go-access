package log

const (
	RequestTraceID string = "request_trace_id"
	Database       string = "database"
	Postgres       string = "postgres"
	Configuration  string = "configuration"
	App            string = "go-access"
	Action         string = "action"
	EventType      string = "event_type"
)
