package log

type Logger interface {
	Info(string, ...interface{})
	Warn(error)
	Error(error)
}
