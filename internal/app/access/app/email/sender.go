package email

type Template struct {
	FileName string
	Data     interface{}
}

type Sender interface {
	SendByTemplate(tpl Template, subject string, receivers []string) error
}
