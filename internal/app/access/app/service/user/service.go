package user

import (
	"go-access/internal/app/access/app/log"
	"go-access/internal/app/access/app/notifier/email"
	"go-access/internal/app/access/domain/model/context"
	"go-access/internal/app/access/domain/model/pagination"
	"go-access/internal/app/access/domain/model/user"

	"emperror.dev/errors"
)

func NewService(
	userService *user.Service,
	emailNotifier *email.Notifier,
	serviceAddress string,
) *Service {
	return &Service{
		userService:    userService,
		emailNotifier:  emailNotifier,
		serviceAddress: serviceAddress,
	}
}

type Service struct {
	emailNotifier  *email.Notifier
	userService    *user.Service
	serviceAddress string
}

const userRegistrationAction = "user registration"

func (s *Service) RegisterUser(ctx *context.Context, u *user.User) error {
	activationCode, err := s.userService.RegisterUser(ctx, u)
	if err != nil {
		return errors.WithDetails(err,
			log.Action, userRegistrationAction,
		)
	}
	activationLink := s.serviceAddress + "/users/activate?code=" + activationCode
	if err := s.emailNotifier.SendUserActivationMessage(activationLink, u.Email); err != nil {
		return errors.WithDetails(err,
			log.Action, userRegistrationAction,
		)
	}
	return nil
}

const userActivationAction = "user activation"

func (s *Service) ActivateUser(ctx *context.Context, activationCode string) error {
	if err := s.userService.ActivateUser(ctx, activationCode); err != nil {
		return errors.WithDetails(err,
			log.Action, userActivationAction,
		)
	}
	return nil
}

const userAuthenticationAction = "user authentication"

func (s *Service) AuthenticateUser(ctx *context.Context, email, password string) (*user.User, error) {
	u, err := s.userService.AuthenticateUser(ctx, email, password)
	if err != nil {
		return nil, errors.WithDetails(err,
			log.Action, userAuthenticationAction,
		)
	}
	return u, nil
}

const gettingUsersPageAction = "getting users page"

func (s *Service) GetUsersPage(ctx *context.Context, page pagination.Page) (user.Collection, pagination.Pagination, error) {
	u, pg, err := s.userService.GetUsersPage(ctx, page)
	if err != nil {
		return nil, pagination.NewEmpty(), errors.WithDetails(err,
			log.Action, gettingUsersPageAction,
		)
	}
	return u, pg, nil
}
