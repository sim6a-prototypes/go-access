package event

type Type string

func (t Type) String() string {
	return string(t)
}

const (
	UserRegistered    Type = "e_userRegistered"
	UserAuthenticated Type = "e_userAuthenticated"
	UserListRetrieved Type = "e_userListRetrieved"
	UserActivated     Type = "e_userActivated"
)
