package event

type Event interface {
	Type() Type
}

type Observer interface {
	Handle(Event)
}

func NewCompositeObserver() *CompositeObserver {
	return &CompositeObserver{
		observers: make([]Observer, 0),
	}
}

type CompositeObserver struct {
	observers []Observer
}

func (co *CompositeObserver) AddObserver(o Observer) {
	co.observers = append(co.observers, o)
}

func (co *CompositeObserver) Handle(evnt Event) {
	for _, o := range co.observers {
		o.Handle(evnt)
	}
}
