package event

type Base struct {
	RequestTraceID string
	_type          Type
}

func (e *Base) SetType(_type Type) {
	e._type = _type
}

func (e *Base) Type() Type {
	return e._type
}
