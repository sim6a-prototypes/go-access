package context

func New() *Context {
	return &Context{}
}

type Context struct {
	RequestTraceID string

	UserGUID string
}
