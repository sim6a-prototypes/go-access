package pagination

import "math"

type Pagination struct {
	PageNumber int
	TotalPages int
}

func NewEmpty() Pagination {
	return Pagination{}
}

func ComputeTotalPages(itemsPerPage, totalItems int) int {
	return int(math.Ceil(float64(totalItems) / float64(itemsPerPage)))
}

type Page struct {
	Number int
	Size   int
}
