package pagination_test

import (
	"go-access/internal/app/access/domain/model/pagination"
	"testing"
)

func TestComputeTotalPages(t *testing.T) {
	type args struct {
		itemsPerPage int
		totalItems   int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "test 1", args: args{
				itemsPerPage: 6,
				totalItems:   20,
			},
			want: 4,
		},
		{
			name: "test 2", args: args{
				itemsPerPage: 5,
				totalItems:   20,
			},
			want: 4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := pagination.ComputeTotalPages(tt.args.itemsPerPage, tt.args.totalItems); got != tt.want {
				t.Errorf("ComputeTotalPages() = %v, want %v", got, tt.want)
			}
		})
	}
}
