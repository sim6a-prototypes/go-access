package errors

func New(message string, _type Type) *Error {
	return &Error{
		message: message,
		_type:   _type,
	}
}

type Error struct {
	message string
	_type   Type
}

func (e *Error) Error() string {
	return e.message
}

func (e *Error) Type() Type {
	return e._type
}
