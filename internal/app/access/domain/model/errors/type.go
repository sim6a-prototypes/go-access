package errors

type Type int

func (t Type) Int() int {
	return int(t)
}

const (
	NotFound       Type = 404
	IncorrectInput Type = 400
)
