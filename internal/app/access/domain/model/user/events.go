package user

import "go-access/internal/app/access/domain/model/event"

/* user registered */

func NewRegisterdEvent(requestTraceId, userFirstName, userLastName, userGUID string) *Registered {
	e := &Registered{
		UserGUID:      userGUID,
		UserFirstName: userFirstName,
		UserLastName:  userLastName,
	}
	e.RequestTraceID = requestTraceId
	e.SetType(event.UserRegistered)
	return e
}

type Registered struct {
	event.Base

	UserFirstName string
	UserLastName  string
	UserGUID      string
}

/* user authenticated */

func NewAuthenticatedEvent(requestTraceId, userGUID string) *Authenticated {
	e := &Authenticated{
		UserGUID: userGUID,
	}
	e.RequestTraceID = requestTraceId
	e.SetType(event.UserAuthenticated)
	return e
}

type Authenticated struct {
	event.Base

	UserGUID string
}

/* user list retrieved */

func NewListRetrievedEvent(requestTraceId, recipientGUID string) *ListRetrieved {
	e := &ListRetrieved{
		RecipientGUID: recipientGUID,
	}
	e.RequestTraceID = requestTraceId
	e.SetType(event.UserListRetrieved)
	return e
}

type ListRetrieved struct {
	event.Base

	RecipientGUID string
}

/* user activated */

func NewActivatedEvent(requestTraceId, userGUID string) *Activated {
	e := &Activated{
		UserGUID: userGUID,
	}
	e.RequestTraceID = requestTraceId
	e.SetType(event.UserActivated)
	return e
}

type Activated struct {
	event.Base

	UserGUID string
}
