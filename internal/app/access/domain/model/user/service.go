package user

import (
	"encoding/hex"
	"go-access/internal/app/access/domain/model/context"
	"go-access/internal/app/access/domain/model/event"
	"go-access/internal/app/access/domain/model/pagination"
	"go-access/internal/pkg/app/guid"
	"go-access/internal/pkg/app/secure"

	emperror "emperror.dev/errors"
)

func NewService(repository Repository, eventObserver event.Observer, encryptionKey string) *Service {
	return &Service{
		repository:    repository,
		eventObserver: eventObserver,
		encryptionKey: encryptionKey,
	}
}

type Service struct {
	repository    Repository
	eventObserver event.Observer
	encryptionKey string
}

func (s *Service) RegisterUser(ctx *context.Context, u *User) (string, error) {
	emailAlreadyExists, err := s.doesEmailExist(u.Email)
	if err != nil {
		return "", emperror.Wrap(err, "cannot check email address for duplication")
	}
	if emailAlreadyExists {
		return "", ErrDuplicate
	}
	g, err := guid.Generate()
	if err != nil {
		return "", emperror.Wrap(err, "cannot generate user guid")
	}
	u.SetGUID(g)
	passwordHash := secure.Sha256(u.Password)
	u.HashPassword(hex.EncodeToString(passwordHash))

	if err := s.repository.Add(u); err != nil {
		return "", emperror.Wrap(err, "cannot store user model")
	}
	encryptedActivationData, err := encryptActivationData(
		ActivationData{UserGUID: u.GUID}, s.encryptionKey)
	if err != nil {
		return "", emperror.Wrap(err, "cannot encrypt user activation data")
	}
	s.eventObserver.Handle(NewRegisterdEvent(ctx.RequestTraceID, u.FirstName, u.LastName, u.GUID))
	return encryptedActivationData, nil
}

func (s *Service) doesEmailExist(email string) (bool, error) {
	users, err := s.repository.FindByEmail(email)
	if err != nil {
		return false, emperror.Wrap(err, "cannot find users by email")
	}
	return !users.IsEmpty(), nil
}

func (s *Service) ActivateUser(ctx *context.Context, activationCode string) error {
	activationData, err := decryptActivationData(activationCode, s.encryptionKey)
	if err != nil {
		return emperror.Wrap(err, "cannot extract activation data")
	}
	u, errSearch := s.repository.FindByGUID(activationData.UserGUID)
	if errSearch != nil {
		return emperror.Wrap(errSearch, "cannot find user by guid")
	}
	u.ConfirmEmail()
	if err := s.repository.Update(u); err != nil {
		return emperror.Wrap(errSearch, "cannot update user model")
	}
	s.eventObserver.Handle(NewActivatedEvent(ctx.RequestTraceID, u.GUID))
	return nil
}

func (s *Service) AuthenticateUser(ctx *context.Context, email, password string) (*User, error) {
	passwordHash := secure.Sha256(password)
	u, err := s.repository.FindByEmailAndPassword(email, hex.EncodeToString(passwordHash))
	if err != nil {
		return nil, emperror.Wrap(err, "cannot find user by email and password")
	}
	s.eventObserver.Handle(NewAuthenticatedEvent(ctx.RequestTraceID, u.GUID))
	return u, nil
}

func (s *Service) GetUsersPage(ctx *context.Context, page pagination.Page) (Collection, pagination.Pagination, error) {
	c, errGettingPage := s.repository.GetPage(page)
	if errGettingPage != nil {
		return nil, pagination.NewEmpty(), emperror.Wrap(errGettingPage, "cannot retrieve users page")
	}
	totalUsers, err := s.repository.Count()
	if err != nil {
		return nil, pagination.NewEmpty(), emperror.Wrap(err, "cannot count users")
	}
	s.eventObserver.Handle(NewListRetrievedEvent(ctx.RequestTraceID, ctx.UserGUID))
	return c, pagination.Pagination{
		PageNumber: page.Number,
		TotalPages: pagination.ComputeTotalPages(page.Size, totalUsers),
	}, nil
}
