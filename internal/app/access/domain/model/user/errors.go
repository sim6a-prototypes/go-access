package user

import "go-access/internal/app/access/domain/model/errors"

var (
	ErrUnknownUser = errors.New("unknown user", errors.NotFound)
	ErrDuplicate   = errors.New("user with such email already exists", errors.IncorrectInput)
)
