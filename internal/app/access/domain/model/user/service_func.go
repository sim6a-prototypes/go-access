package user

import (
	"encoding/json"
	"go-access/internal/pkg/app/secure"
)

func encryptActivationData(activationData ActivationData, encryptionKey string) (string, error) {
	// TODO: use jsoniter
	activationDataInBytes, errMarshalling := json.Marshal(&activationData)
	if errMarshalling != nil {
		return "", errMarshalling
	}
	encrypted, errEnctyption := secure.Encrypt(string(activationDataInBytes), encryptionKey)
	if errEnctyption != nil {
		return "", errEnctyption
	}
	return encrypted.String(), nil
}

func decryptActivationData(encryptedActivationData, encryptionKey string) (ActivationData, error) {
	encryptedActivationDataHex, errHexDecoding := secure.NewHex(encryptedActivationData)
	if errHexDecoding != nil {
		return ActivationData{}, errHexDecoding
	}
	activationDataString, errDecryption := secure.Decrypt(encryptedActivationDataHex, encryptionKey)
	if errDecryption != nil {
		return ActivationData{}, errDecryption
	}
	activationData := ActivationData{}
	// TODO: use jsoniter
	if errUnmarshalling := json.Unmarshal([]byte(activationDataString), &activationData); errUnmarshalling != nil {
		return ActivationData{}, errUnmarshalling
	}
	return activationData, nil
}
