package user

import (
	"go-access/internal/app/access/domain/model/pagination"
)

type Repository interface {
	FindByGUID(string) (*User, error)
	FindByEmail(string) (Collection, error)
	FindByEmailAndPassword(string, string) (*User, error)

	Add(*User) error
	Update(*User) error
	GetPage(pagination.Page) (Collection, error)
	Count() (int, error)
}
