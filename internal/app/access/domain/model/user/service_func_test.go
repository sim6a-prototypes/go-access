package user

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_encryptAndDecryptActivationData(t *testing.T) {
	const encryptionKey = "key^?"
	data := ActivationData{
		UserGUID: "016cf4eb-3aeb-0684-6f17-9fe1ee876865",
	}
	encryped, errEncryption := encryptActivationData(data, encryptionKey)
	assert.NoError(t, errEncryption)

	decrypted, errDecryption := decryptActivationData(encryped, encryptionKey)
	assert.NoError(t, errDecryption)

	assert.Equal(t, data.UserGUID, decrypted.UserGUID)
}
