package user

import "github.com/jinzhu/gorm"

func New(firstName, lastName, password, email string) *User {
	return &User{
		FirstName: firstName,
		LastName:  lastName,
		Password:  password,
		Email:     email,
	}
}

type User struct {
	gorm.Model

	GUID           string `gorm:"type:varchar(36)"`
	Email          string `gorm:"type:varchar(100)"`
	Password       string `gorm:"type:varchar(64)"`
	FirstName      string `gorm:"type:varchar(50)"`
	LastName       string `gorm:"type:varchar(50)"`
	EmailConfirmed bool

	Roles []*Role `gorm:"-"`
}

func (u *User) SetGUID(guid string) {
	u.GUID = guid
}

func (u *User) HashPassword(hash string) {
	u.Password = hash
}

func (u *User) ConfirmEmail() {
	u.EmailConfirmed = true
}

func (u *User) IsActive() bool {
	return u.EmailConfirmed
}

type Collection []*User

func (c Collection) IsEmpty() bool {
	return len(c) == 0
}

type ActivationData struct {
	UserGUID string `json:"guid"`
}
