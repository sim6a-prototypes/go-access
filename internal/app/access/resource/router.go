package resource

import (
	"context"
	"go-access/internal/app/access/app/log"
	"go-access/internal/app/access/app/service/user"
	"go-access/internal/app/access/resource/middleware/errors"
	logMiddleware "go-access/internal/app/access/resource/middleware/log"
	userResource "go-access/internal/app/access/resource/user"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "go-access/doc"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	emperror "emperror.dev/errors"

	"github.com/gin-gonic/gin"
)

// @title go-access API
// @version 1.0
// @description User identity service.

// @host localhost:8080
// @BasePath /

func NewRestAPI(cfg Config) *gin.Engine {
	gin.SetMode(cfg.GinMode)
	return gin.New()
}

type Config struct {
	Address string
	GinMode string
}

func ConfigureRestAPI(router *gin.Engine, cfg Config, logger log.Logger) {
	errorCatcherMiddleware := errors.NewCatcher(logger)
	loggerMw := logMiddleware.NewLogger(logger)

	router.Use(errorCatcherMiddleware.Catch)
	router.Use(loggerMw.LogRequest)
	router.GET("/info", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"webServer": "gin",
			"mode":      cfg.GinMode,
		})
	})
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}

func InjectServices(router *gin.Engine, userService *user.Service) {
	userHandler := userResource.NewHandler(userService)
	userResource.SetRoutes(router, userHandler)
}

func RunRestAPI(router *gin.Engine, cfg Config, logger log.Logger) {
	server := &http.Server{
		Addr:    cfg.Address,
		Handler: router,
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			logger.Error(emperror.WrapWithDetails(err, log.App+" listening"))
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	logger.Info(log.App + " is ready to accept requests")
	<-quit
	logger.Info("shutdown " + log.App + " ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		logger.Error(emperror.WrapWithDetails(err, log.App+" shutdown"))
	}

	logger.Info(log.App + " exiting")
}
