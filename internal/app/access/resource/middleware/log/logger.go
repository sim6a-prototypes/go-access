package log

import (
	"bytes"
	"go-access/internal/app/access/app/log"
	"go-access/internal/app/access/domain/model/context"
	"go-access/internal/pkg/std/conv"
	"io/ioutil"
	"net/http"

	"emperror.dev/errors"
	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
)

const (
	RequestContext string = "__reqContext"
)

func NewLogger(logger log.Logger) *Logger {
	return &Logger{
		logger: logger,
	}
}

type Logger struct {
	logger log.Logger
}

func (l *Logger) LogRequest(c *gin.Context) {
	requestContext := context.Context{
		RequestTraceID: newRequestTraceID(),
	}
	l.logger.Info("request accepted",
		log.RequestTraceID, requestContext.RequestTraceID,
		"method", c.Request.Method,
		"url_path", c.Request.URL.Path,
		"qurey_strng", c.Request.URL.Query().Encode(),
		"request_body", l.readRequestBody(c.Request, requestContext.RequestTraceID),
	)
	c.Set(RequestContext, &requestContext)
}

func newRequestTraceID() string {
	return xid.New().String()
}

func (l *Logger) readRequestBody(request *http.Request, requestTraceID string) string {
	const (
		readingError = "__READING_ERROR__"
	)
	contents, err := ioutil.ReadAll(request.Body)
	if err != nil {
		l.logBodyReadingError(err, requestTraceID)
		return readingError
	}
	if len(contents) == 0 {
		return ""
	}
	request.Body = ioutil.NopCloser(bytes.NewReader(contents))
	compactedContent, err := conv.CompactJSON(contents)
	if err != nil {
		l.logBodyReadingError(err, requestTraceID)
		return readingError
	}
	return compactedContent
}

func (l *Logger) logBodyReadingError(err error, requestTraceID string) {
	l.logger.Warn(errors.WrapWithDetails(err, "request body reading",
		log.RequestTraceID, requestTraceID),
	)
}
