package errors

import (
	"go-access/internal/app/access/app/log"
	"go-access/internal/app/access/domain/model/context"
	domainErrors "go-access/internal/app/access/domain/model/errors"
	logMiddleware "go-access/internal/app/access/resource/middleware/log"
	"go-access/internal/app/access/resource/validation"
	"net/http"

	ozzoValidation "github.com/go-ozzo/ozzo-validation/v4"

	"emperror.dev/errors"
	"github.com/gin-gonic/gin"
)

func NewCatcher(logger log.Logger) *Catcher {
	return &Catcher{
		logger: logger,
	}
}

type Catcher struct {
	logger log.Logger
}

func (ec *Catcher) Catch(c *gin.Context) {
	c.Next()
	if len(c.Errors) == 0 {
		return
	}

	for _, err := range c.Errors {
		ec.handleError(err, c)
	}
}

func (ec *Catcher) handleError(err error, c *gin.Context) {
	ginErr := err.(*gin.Error)
	ec.handleGinError(ginErr.Err, c)
}

func (ec *Catcher) handleGinError(err error, c *gin.Context) {
	switch parsedErr := err.(type) {
	case *validation.BindingError:
		ec.handleBindingError(parsedErr, c)
	case ozzoValidation.Errors:
		ec.handleValidationErrors(parsedErr, c)
	default:
		cause := errors.Cause(err)
		if domainError, ok := cause.(*domainErrors.Error); ok {
			ec.handleDomainError(domainError, c)
		} else {
			ec.handleInternalError(err, c)
		}
	}
}

func (ec *Catcher) handleDomainError(err *domainErrors.Error, c *gin.Context) {
	httpErr := ErrorViewModel{
		Error:  err.Error(),
		Status: err.Type().Int(),
	}
	c.JSON(err.Type().Int(), &httpErr)
}

func (ec *Catcher) handleValidationErrors(errs ozzoValidation.Errors, c *gin.Context) {
	errorList := make([]string, 0, len(errs))
	for _, e := range errs {
		errorList = append(errorList, e.Error())
	}
	httpErr := ErrorViewModel{
		Error:   "validation error",
		Details: errorList,
		Status:  http.StatusBadRequest,
	}
	c.JSON(http.StatusBadRequest, &httpErr)
}

func (ec *Catcher) handleBindingError(err *validation.BindingError, c *gin.Context) {
	httpErr := ErrorViewModel{
		Error:  err.Error(),
		Status: http.StatusBadRequest,
	}
	c.JSON(http.StatusBadRequest, &httpErr)
}

func (ec *Catcher) handleInternalError(err error, c *gin.Context) {
	httpErr := ErrorViewModel{
		Error:  "internal server error",
		Status: http.StatusInternalServerError,
	}
	c.JSON(http.StatusInternalServerError, &httpErr)
	requestContext := c.MustGet(logMiddleware.RequestContext).(*context.Context)
	ec.logger.Error(errors.WithDetails(err,
		log.RequestTraceID, requestContext.RequestTraceID,
	))
}
