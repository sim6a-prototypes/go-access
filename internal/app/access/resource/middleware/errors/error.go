package errors

type ErrorViewModel struct {
	Error   string   `json:"error"`
	Details []string `json:"details,omitempty"`
	Status  int      `json:"status"`
}
