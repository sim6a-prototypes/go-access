package user

import (
	"go-access/internal/app/access/resource/pagination"

	"github.com/gin-gonic/gin"
)

func SetRoutes(router *gin.Engine, handler *Handler) {
	u := router.Group("/users")
	{
		u.GET("/activate", bindActivationModel, handler.ActivateUser)
		u.GET("/", pagination.Bind, handler.GetUsersPage)
		u.POST("/auth", bindAuthenticationModel, handler.AuthenticateUser)
		u.POST("/", bindRegistrationModel, validateRegistrationModel, handler.RegisterUser)
	}
}
