package user

import (
	"go-access/internal/app/access/resource/validation"

	"github.com/gin-gonic/gin"
	ozzoValidation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

/* Registration */

func bindRegistrationModel(c *gin.Context) {
	var model RegistrationViewModel
	if err := c.ShouldBindJSON(&model); err != nil {
		c.Abort()
		_ = c.Error(validation.NewInvalidJSONSchemeError())
		return
	}
	c.Set(gin.BindKey, &model)
}

func validateRegistrationModel(c *gin.Context) {
	model := c.MustGet(gin.BindKey).(*RegistrationViewModel)
	err := ozzoValidation.ValidateStruct(model,
		ozzoValidation.Field(&model.Email,
			is.Email.Error("incorrect email format"),
			ozzoValidation.Required.Error("email required")),

		ozzoValidation.Field(&model.FirstName,
			ozzoValidation.Required.Error("first name required")),

		ozzoValidation.Field(&model.LastName,
			ozzoValidation.Required.Error("last name required")),

		ozzoValidation.Field(&model.Password,
			ozzoValidation.Required.Error("password required"),
			ozzoValidation.By(validation.ValidatePasword),
		),
	)
	if err != nil {
		c.Abort()
		_ = c.Error(err)
		return
	}
}

/* Authentication */

func bindAuthenticationModel(c *gin.Context) {
	var model AuthenticationViewModel
	if err := c.ShouldBindJSON(&model); err != nil {
		c.Abort()
		_ = c.Error(validation.NewInvalidJSONSchemeError())
		return
	}
	c.Set(gin.BindKey, &model)
}

/* Activation */

func bindActivationModel(c *gin.Context) {
	var model ActivationViewModel
	if err := c.ShouldBindQuery(&model); err != nil {
		c.Abort()
		_ = c.Error(validation.NewInvalidQueryStringError())
		return
	}
	c.Set(gin.BindKey, &model)
}
