package user

import (
	"go-access/internal/app/access/domain/model/user"
	"go-access/internal/app/access/resource/pagination"
)

type ActivationViewModel struct {
	Code string `form:"code"`
}

type RegistrationViewModel struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Password  string `json:"password"`
	Email     string `json:"email"`
}

func (m *RegistrationViewModel) toUser() *user.User {
	return user.New(m.FirstName, m.LastName, m.Password, m.Email)
}

type AuthenticationViewModel struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func newViewModel(u *user.User) UserViewModel {
	return UserViewModel{
		GUID:      u.GUID,
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Email:     u.Email,
	}
}

type UserViewModel struct {
	GUID      string `json:"guid"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
}

type UserViewModelCollection []UserViewModel

func newViewModelCollection(users user.Collection) UserViewModelCollection {
	collection := make(UserViewModelCollection, 0, len(users))
	for _, u := range users {
		collection = append(collection, newViewModel(u))
	}
	return collection
}

type UserViewModelPagedCollection struct {
	Users      UserViewModelCollection        `json:"users"`
	Pagination pagination.PaginationViewModel `json:"pagination"`
}
