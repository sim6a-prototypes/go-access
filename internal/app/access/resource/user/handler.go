package user

import (
	"go-access/internal/app/access/app/service/user"
	"go-access/internal/app/access/domain/model/context"
	"go-access/internal/app/access/resource/middleware/log"
	"go-access/internal/app/access/resource/pagination"
	"net/http"

	// model for online documentation
	_ "go-access/internal/app/access/resource/middleware/errors"

	"github.com/gin-gonic/gin"
)

func NewHandler(svc *user.Service) *Handler {
	return &Handler{
		svc: svc,
	}
}

type Handler struct {
	svc *user.Service
}

// @Summary Get user list.
// @Description Get paged user list.
// @Param page query number true "Page number" minimum(1)
// @Param size query number true "Items per page" minimum(1)
// @Tags users
// @Accept json
// @Produce json
// @Success 200 {object} UserViewModelPagedCollection
// @Failure 400 {object} errors.ErrorViewModel
// @Router /users [get]
func (h *Handler) GetUsersPage(c *gin.Context) {
	ctx := c.MustGet(log.RequestContext).(*context.Context)
	pageVM := c.MustGet(gin.BindKey).(*pagination.PageView)
	userVMs, pg, err := h.svc.GetUsersPage(ctx, pageVM.ToPage())
	if err != nil {
		c.Abort()
		_ = c.Error(err)
		return
	}
	c.JSON(http.StatusOK, &UserViewModelPagedCollection{
		Users:      newViewModelCollection(userVMs),
		Pagination: pagination.New(pg),
	})
}

// @Summary Authenticate a user.
// @Description Get user authentication data by login and password.
// @Param Authentication data body AuthenticationViewModel true "Body"
// @Tags users
// @Accept json
// @Produce json
// @Success 200 {object} UserViewModel
// @Failure 400 {object} errors.ErrorViewModel
// @Router /users/auth [post]
func (h *Handler) AuthenticateUser(c *gin.Context) {
	authenticationVM := c.MustGet(gin.BindKey).(*AuthenticationViewModel)
	ctx := c.MustGet(log.RequestContext).(*context.Context)
	u, err := h.svc.AuthenticateUser(ctx, authenticationVM.Email, authenticationVM.Password)
	if err != nil {
		c.Abort()
		_ = c.Error(err)
		return
	}
	c.JSON(http.StatusOK, newViewModel(u))
}

// @Summary Register a user.
// @Description Add user data to database.
// @Param Registration data body RegistrationViewModel true "Body"
// @Tags users
// @Accept json
// @Produce json
// @Success 200 {object} UserViewModel
// @Failure 400 {object} errors.ErrorViewModel
// @Router /users [post]
func (h *Handler) RegisterUser(c *gin.Context) {
	registrationVM := c.MustGet(gin.BindKey).(*RegistrationViewModel)
	ctx := c.MustGet(log.RequestContext).(*context.Context)
	u := registrationVM.toUser()
	if err := h.svc.RegisterUser(ctx, u); err != nil {
		c.Abort()
		_ = c.Error(err)
		return
	}
	c.JSON(http.StatusCreated, newViewModel(u))
}

// @Summary Activate a user.
// @Description Confirm user email.
// @Param code query number true "Activation code"
// @Tags users
// @Accept json
// @Produce json
// @Success 200 {string} string	"user activated"
// @Failure 400 {object} errors.ErrorViewModel
// @Router /users/active [get]
func (h *Handler) ActivateUser(c *gin.Context) {
	activationVM := c.MustGet(gin.BindKey).(*ActivationViewModel)
	ctx := c.MustGet(log.RequestContext).(*context.Context)
	if err := h.svc.ActivateUser(ctx, activationVM.Code); err != nil {
		c.Abort()
		_ = c.Error(err)
		return
	}
	c.String(http.StatusOK, "user activated")
}
