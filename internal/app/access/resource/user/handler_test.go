package user_test

import (
	"bytes"
	notifier "go-access/internal/app/access/app/notifier/email"
	appUser "go-access/internal/app/access/app/service/user"
	"go-access/internal/app/access/domain/model/event"
	"go-access/internal/app/access/domain/model/user"
	"go-access/internal/app/access/mock/email"
	"go-access/internal/app/access/mock/log"
	testUser "go-access/internal/app/access/mock/user"
	"go-access/internal/app/access/resource"
	"go-access/internal/pkg/std/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func setupRegistrationRouter() *gin.Engine {
	const (
		encryptionKey  = "key^?"
		serviceAddress = "http://test.access.com"
		ginMode        = "test"
	)

	logger := &log.FakeLogger{}

	emailSender := &email.FakeSender{}
	emailNotifier := notifier.NewNotifier(notifier.Config{}, emailSender, logger)
	compositeObserver := event.NewCompositeObserver()
	userRepository := testUser.NewFakeRepository()
	userService := user.NewService(userRepository, compositeObserver, encryptionKey)
	appUserService := appUser.NewService(userService, emailNotifier, serviceAddress)

	serverConfig := resource.Config{GinMode: ginMode}
	restAPI := resource.NewRestAPI(serverConfig)
	resource.ConfigureRestAPI(restAPI, serverConfig, logger)
	resource.InjectServices(restAPI, appUserService)
	return restAPI
}

var registrationTests = []struct {
	init           func(*http.Request)
	url            string
	method         string
	bodyData       string
	expectedCode   int
	responseRegexp string
	msg            string
}{
	{
		url:    "/users/",
		method: "POST",
		bodyData: json.CompactString(`
		{
			"firstName":"Alex",
			"lastName":"Blade",
			"password":"123456789",
			"email":"user@yandex.ru"
		}`),
		expectedCode: http.StatusCreated,
		responseRegexp: json.CompactString(`
		{
			"guid":"(.{8}-.{4}-.{4}-.{4}-.{12})",
			"firstName":"Alex",
			"lastName":"Blade",
			"email":"user@yandex.ru"
		}`),
		msg: "user must be registered",
	},
	{
		url:    "/users/",
		method: "POST",
		bodyData: json.CompactString(`
		{
			"firstName":"Alex",
			"lastName":"Blade",
			"password":"",
			"email":"user@yandex.ru"
		}`),
		expectedCode: http.StatusBadRequest,
		responseRegexp: json.CompactString(`
		{
			"error":"validation error",
			"details":\["password required"\],
			"status":400
		}`),
		msg: "no password",
	},
	{
		url:    "/users/",
		method: "POST",
		bodyData: json.CompactString(`
		{
			"firstName":"Alex",
			"lastName":"Blade",
			"password":"123",
			"email":"user@yandex.ru"
		}`),
		expectedCode: http.StatusBadRequest,
		responseRegexp: json.CompactString(`
		{
			"error":"validation error",
			"details":\["password must consists of alphanumeric characters and predefined wild characters. length must be at least 8 characters and not more than 15 characters."\],
			"status":400
		}`),
		msg: "invalid password",
	},
}

func TestUserRegistration(t *testing.T) {
	should := assert.New(t)
	router := setupRegistrationRouter()

	for _, tt := range registrationTests {
		bodyData := tt.bodyData
		req, err := http.NewRequest(tt.method, tt.url, bytes.NewBufferString(bodyData))
		req.Header.Set("Content-Type", "application/json")
		should.NoError(err)

		w := httptest.NewRecorder()
		router.ServeHTTP(w, req)

		should.Equal(tt.expectedCode, w.Code, "Response Status - "+tt.msg)
		should.Regexp(tt.responseRegexp, w.Body.String(), "Response Content - "+tt.msg)
	}
}
