package pagination

import (
	"go-access/internal/app/access/domain/model/pagination"
	"go-access/internal/app/access/resource/validation"

	"github.com/gin-gonic/gin"
)

type PageView struct {
	Number int `form:"page"`
	Size   int `form:"size"`
}

func (p *PageView) Correct() {
	if p.Number <= 0 {
		p.Number = 1
	}
	if p.Size <= 0 {
		p.Size = 5
	}
}

func (p *PageView) ToPage() pagination.Page {
	return pagination.Page{
		Number: p.Number,
		Size:   p.Size,
	}
}

func Bind(c *gin.Context) {
	var page PageView
	if err := c.ShouldBindQuery(&page); err != nil {
		c.Abort()
		_ = c.Error(validation.NewInvalidQueryStringError())
		return
	}
	page.Correct()
	c.Set(gin.BindKey, &page)
}

type PaginationViewModel struct {
	PageNumber int `json:"page"`
	TotalPages int `json:"totalPages"`
}

func New(pg pagination.Pagination) PaginationViewModel {
	return PaginationViewModel{
		PageNumber: pg.PageNumber,
		TotalPages: pg.TotalPages,
	}
}
