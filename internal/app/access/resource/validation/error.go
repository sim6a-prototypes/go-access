package validation

type Errors []error

func (ee Errors) Error() string {
	return "validation errors"
}

type BindingError struct {
	message string
}

func (e *BindingError) Error() string {
	return e.message
}

func NewInvalidJSONSchemeError() *BindingError {
	return &BindingError{
		message: "invalid json scheme",
	}
}

func NewInvalidQueryStringError() *BindingError {
	return &BindingError{
		message: "invalid query string format",
	}
}
