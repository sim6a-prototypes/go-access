package validation

import (
	"errors"
	"go-access/internal/pkg/app/regexp"
)

func ValidatePasword(value interface{}) error {
	password, _ := value.(string)
	if !regexp.Password.MatchString(password) {
		return errors.New("password must consists of alphanumeric characters and predefined wild characters. length must be at least 8 characters and not more than 15 characters.")
	}
	return nil
}
