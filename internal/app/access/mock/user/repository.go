package user

import (
	"go-access/internal/app/access/domain/model/pagination"
	"go-access/internal/app/access/domain/model/user"
	"go-access/internal/app/access/infrastructure/db/postgres"
)

func NewFakeRepository() *FakeRepository {
	return &FakeRepository{
		users: make([]*user.User, 0),
	}
}

type FakeRepository struct {
	users         []*user.User
	internalError error
}

func (r *FakeRepository) ThrowInternalErrorAlways(err error) {
	r.internalError = err
}

func (r *FakeRepository) SuppressInternalErrorAlways() {
	r.internalError = nil
}

func (r *FakeRepository) Clear() {
	r.users = make([]*user.User, 0)
}

func (r *FakeRepository) FindByGUID(guid string) (*user.User, error) {
	if r.internalError != nil {
		return nil, r.internalError
	}

	var found *user.User
	for _, usr := range r.users {
		if usr.GUID == guid {
			found = usr
			break
		}
	}

	if found == nil {
		return nil, user.ErrUnknownUser
	}
	return found, nil
}

func (r *FakeRepository) FindByEmail(email string) (user.Collection, error) {
	if r.internalError != nil {
		return nil, r.internalError
	}

	found := make(user.Collection, 0)
	for _, usr := range r.users {
		if usr.Email == email {
			found = append(found, usr)
		}
	}

	return found, nil
}

func (r *FakeRepository) FindByEmailAndPassword(email, password string) (*user.User, error) {
	if r.internalError != nil {
		return nil, r.internalError
	}

	var found *user.User
	for _, usr := range r.users {
		if usr.Email == email && usr.Password == password {
			found = usr
			break
		}
	}

	if found == nil {
		return nil, user.ErrUnknownUser
	}
	return found, nil
}

func (r *FakeRepository) Add(u *user.User) error {
	if r.internalError != nil {
		return r.internalError
	}

	r.users = append(r.users, u)
	return nil
}

func (r *FakeRepository) Update(u *user.User) error {
	if r.internalError != nil {
		return r.internalError
	}

	found, err := r.FindByGUID(u.GUID)
	if err != nil {
		return err
	}

	found.Email = u.Email
	found.EmailConfirmed = u.EmailConfirmed
	found.FirstName = u.FirstName
	found.LastName = u.LastName
	found.Password = u.Password
	return nil
}

func (r *FakeRepository) GetPage(page pagination.Page) (user.Collection, error) {
	if r.internalError != nil {
		return nil, r.internalError
	}

	limit, offset := postgres.ComputeLimitAndOffset(page.Number, page.Size)
	if offset >= len(r.users) {
		return make(user.Collection, 0), nil
	}
	if limit+offset >= len(r.users) {
		return r.users[offset:], nil
	}
	return r.users[offset : offset+limit], nil
}

func (r *FakeRepository) Count() (int, error) {
	if r.internalError != nil {
		return 0, r.internalError
	}

	return len(r.users), nil
}
