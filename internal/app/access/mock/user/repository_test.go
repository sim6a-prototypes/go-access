package user

import (
	"go-access/internal/app/access/domain/model/pagination"
	"go-access/internal/app/access/domain/model/user"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestFakeRepository_GetPage(t *testing.T) {
	should := assert.New(t)
	mustHave := require.New(t)

	r := NewFakeRepository()

	r.Add(&user.User{FirstName: "Billy-1"})
	r.Add(&user.User{FirstName: "Billy-2"})
	r.Add(&user.User{FirstName: "Billy-3"})
	r.Add(&user.User{FirstName: "Billy-4"})
	r.Add(&user.User{FirstName: "Billy-5"})

	users, _ := r.GetPage(pagination.Page{Number: 2, Size: 2})

	mustHave.Len(users, 2)
	should.Equal("Billy-3", users[0].FirstName)
	should.Equal("Billy-4", users[1].FirstName)
}
