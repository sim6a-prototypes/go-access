package email

import "go-access/internal/app/access/app/email"

type FakeSender struct {
	internalError  error
	wasCalledTimes int
}

func (s *FakeSender) WasCalledTimes() int {
	return s.wasCalledTimes
}

func (s *FakeSender) SendByTemplate(tpl email.Template, subject string, receivers []string) error {
	s.wasCalledTimes++

	if s.internalError != nil {
		return s.internalError
	}
	return nil
}
