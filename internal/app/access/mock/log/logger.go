package log

type FakeLogger struct{}

func (FakeLogger) Info(string, ...interface{}) {}
func (FakeLogger) Warn(error)                  {}
func (FakeLogger) Error(error)                 {}
