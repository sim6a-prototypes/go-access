.PHONY: run-dev
run-dev: doc
	docker-compose -f ./deployments/dev/docker-compose.dev.yml up --build

.PHONY: build
build:
	go build -tags=jsoniter -o /tmp/access cmd/access/main.go

.PHONY: test
test:
	go test ./...

.PHONY: doc
doc:
	swag init -g internal/app/access/resource/router.go -o doc