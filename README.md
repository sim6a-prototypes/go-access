# go-Access

## Dev Tools
Postgres client `sudo apt-get install -y postgresql-client`

## Postgres Test Data
Use `~/.pgpass` file with line like `127.0.0.1:5432:dbname:dbuser:p@ssword` to connect to postgres from test scripts

## Benchmark
You can use this tool `https://pypi.org/project/boom/`

## Building
`-tags=jsoniter` used to use jsoniter in gin.